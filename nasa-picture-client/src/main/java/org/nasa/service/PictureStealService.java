package org.nasa.service;

import org.nasa.dto.StealPictureRequestDto;

public interface PictureStealService {

  String stealPicture(StealPictureRequestDto request);

}
