package org.nasa.client;

import org.nasa.dto.StealPictureRequestDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "nasa-picture-stealer", url = "${nasa-picture-stealer-host}")
public interface StealPictureClient {

  @PostMapping("/pictures/steal")
  ResponseEntity<Void> stealPictures(@RequestBody StealPictureRequestDto request);

}
