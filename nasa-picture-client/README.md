## Endpoint in the file
[stealer.http](../assets/scatches/stealer.http)

## Enable nginx ingress controller in the cluster
```shell
minikube addons enable ingress
```

## Get ingress controller pods
```shell
kubectl get pod -n ingress-nginx
```

## Check the ingress response
```shell
 curl -Ivk http://stealer.tech/
```

### For domain configured in ingress file to work
### need to configure DNS mapping to map the Ingress external IP to the domain name



# Future development
#### For the future deployment to the real kubernetes cluster
There are many managed providers such as: AWS EKS, Google GKE, Microsoft AKS and CIVO
CIVO is the cheapest one to start with - https://www.civo.com/

#### Security. 
To use HTTPS will need:
 - to use cert-manager - https://cert-manager.io/ to manage the Certificate Authority secrets
   Useful diagram about how it works - https://github.com/christianlempa/videos/tree/main/self-signed-certificates-in-kubernetes
 - to use one of the Certificate Authorities like  Let's Encrypt, HashiCorp Vault, and Venafi
 - create ClusterIssuer manifest file
 - Configure Ingress manifest file to use Certificate Authority host
   with ACME protocol or if it is allowed by the authority provider some other protocol
This will temporarily create deployments, services, pods, certificate, pods resources
in order to validate the certificate by checking the owner of the host and maybe other validations
to signe the certificate and therefor need to allocate a bit more memory and CPU usages for the cluster nodes.