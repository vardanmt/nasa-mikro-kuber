# Nasa picture stealer project using 
 - ### Microservices
 - ### Secured with OAuth0
 - ### Kubernetes manifest file (for now without tls - needed deploy to real cluster)
 - ### Docker files in the root of each microservice



# Helpful kubernetes commands

## Load the image to the minikube docker registry
```
minikube image load nasa-picture-stealer:latest
```

## Build a docker image in our local machine and then deploy them to the minikube environment.
## Helps to connect your Docker CLI to the docker daemon inside the VM you need to run : eval $(minikube docker-env)
```
eval $(minikube -p minikube docker-env)
```

## Get all pods
```
kubectl get pods -n nasa-client-dev
```

## Run the configuration files
```
kubectl apply -f ./kubernetes/dev/service.yaml
```

## Get pod logs
```
kubectl logs nasa-picture-client-deployment-6764c596dc-6vchd -n nasa-client-dev
```

## Assign the external port to the service. Expose the service with NodePort be accessible from the browser
```
minikube service nasa-client-service -n nasa-client-dev
```

## Delete the pod
```
kubectl delete pod nasa-picture-client-deployment-659597f8cc-sxqpp -n nasa-client-dev
```