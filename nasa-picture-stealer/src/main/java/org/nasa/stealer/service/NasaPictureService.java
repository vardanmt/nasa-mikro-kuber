package org.nasa.stealer.service;

import org.nasa.stealer.dto.StealPictureRequestDto;

public interface NasaPictureService {
  void savePictures(StealPictureRequestDto request);

}
