package org.nasa.stealer.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class StealPictureRequestDto {

  Integer sol;

}
