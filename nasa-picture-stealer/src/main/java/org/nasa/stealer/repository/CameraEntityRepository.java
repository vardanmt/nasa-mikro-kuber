package org.nasa.stealer.repository;

import org.nasa.stealer.entity.CameraEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CameraEntityRepository extends JpaRepository<CameraEntity, Integer> {

}
